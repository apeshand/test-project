# Памятка
> скорее самому себе как всем этим "СЧАСТЬЕМ" воспользоваться

## Задание

    1. Создать через terraform 3 vps в digitalocean/aws/google cloud (где нравится)
    2. Через ansible установить на них docker
    3. Создать на нодах kubernetes кластер с помощью rke
    4. Запустить в кластере deployment с обычным nginx’ом внутри.
    5. Оформить все конфигурации в виде git репозитория

## Подготовка

Нам понадобятся следущие инструменты:

1. [Terraform](https://learn.hashicorp.com/tutorials/terraform/install-cli)
2. [Ansible](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html)
3. [RKE](https://rancher.com/docs/rke/latest/en/installation/)
4. [kubectl](https://kubernetes.io/docs/tasks/tools/)

## Выполнение задания

1. В каталоге `00.tf`:
    Первым делом создайте сервисную учётную запись в GCP. Ссылка на руководство: [*Terraform GCP get started*](https://learn.hashicorp.com/tutorials/terraform/google-cloud-platform-build?in=terraform/gcp-get-started#set-up-gcp). Пройдя по пунктам рукодства получите ключ от сервисной учётной записи, он нам ещё пригодится в дальнейшем.
    <!-- *Так же упоминание об этом находится в файле `terraform.tfvars.example`.* -->
    Далее укажите Ваш GCP ID проекта и файл сервисного аккаунта в `terraform.tfvars`.
    > _Опционально_: проверьте файл `variable.tf` и исправьте, к примеру, имя создаваемого пользователя

    Можем приступать к разворачиванию виртуальных машин:

    ```bash
     $ terraform init
     $ terraform apply --auto-approve
    ```

2. В каталоге `01.ans`:
    Скопируйте ключ сервисного аккаунта в папку `iventory/*`. Для того, чтобы воспользоваться динамической инвентарной книгой, необходимо поправить файл `inventory/gcp.yaml.example`, удалив в названии `.exmaple` и поправить содержимое согласно подсказке внутри. Так же необходимо установить пару зависимостей через `pip`:

    ```bash
    $ sudo pip install requests google-auth
    ```

    Теперь можно приступать к установке `Docker CE` на удалённых хостах:

    > _Опционально_: Если пуктом раньше Вы изменяли имя пользователя, то его следует изменить в `playbook.yml`

    ```bash
    $ ansible-playbook playbook.yml
    ```
3. В каталоге `02.rke`:
    Идём по руководству [*Creating the Cluster Configuration File*](https://rancher.com/docs/rke/latest/en/installation/#creating-the-cluster-configuration-file) 

    ИЛИ

    правим файл конфигурации `cluster.yml.example`(удалив из него часть `.example`) вручную, изменив строки конфигурации, значения которых соответствуют шаблону `< * >`(1. скобки `<>` не нужны; 2. не рекомендую этот способ, но почему бы и нет?).

    Выполняем команду ниже и дожидаемся сообщения вида:

    ```bash
    $ rke up
    ```

    > INFO[0101] Finished building Kubernetes cluster successfully

    Что говорит о том, что кластер поднят успешно. В директории появится файл `kube_config_cluster.yml`, который понадобится в следующем пункте.

4. Тут же:
    __kubectl__:
    Лично я столкнулся с проблемой того, что использование переменной `KUBECONFIG` и явным указанием на файл конфига не принесло ровным счётом ни какого результата. А потому тут два варианта: использовать `kubectl --kubeconfig kube_config_cluster.yml команда` или переместить файл `kube_config_cluster.yml` в директорию и переименовать - `~/.kube/config`.

    Далее, мне по заданию необходимо сделать ***deployment*** с `nginx`. В руководстве к `kubernetes` имеется пример такого ***deployment***, так как опыта работы с `kubernetes` у меня нет, то доверимся документации и воспользуемся предоставленным примером. [_Ссылка на пример_](https://kubernetes.io/docs/tasks/run-application/run-stateless-application-deployment/#creating-and-exploring-an-nginx-deployment)

    Выполним следующие команды:

    ```bash
    $ kubectl apply -f application/deployment.yml
    $ kubectl describe deployment nginx-deployment
    $ kubectl get pods -l app=nginx
    ```
    Вывод последней команды:

    ```bash
    NAME                                READY   STATUS    RESTARTS   AGE
    nginx-deployment-66b6c48dd5-2f2ld   1/1     Running   0          12h
    nginx-deployment-66b6c48dd5-fr5g6   1/1     Running   0          12h
    nginx-deployment-66b6c48dd5-zrz7t   1/1     Running   0          12h
    ```

    ![Скриншот подтверждающий вывод](./docs/screenshot.png)