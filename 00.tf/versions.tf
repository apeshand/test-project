/* 
  Используем актуальные версии провайдеров с сайта HashiCorp
  https://registry.terraform.io/browse/providers
  required_version - надеюсь мы используем актуальную версию, да?) 
    $ terraform --version
    $   Terraform v1.0.7
    $   on linux_amd64
*/
terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "3.85.0"
    }
    local = {
      source  = "hashicorp/local"
      version = "2.1.0"
    }
    tls = {
      source  = "hashicorp/tls"
      version = "3.1.0"
    }
  }
  required_version = ">= 0.13"
}