variable "project" {
  description = "Название проекта с его ID"
  default = "My-GCP-project-000000"
}

variable "credentials_file" {
  description = "Файл данных сервисной учётной записи GCP"
  default = "service-account.json"
}

variable "node_count" {
  description = "Кол-во хостов, которые мы создадим"
  default = "3"
}

variable "machine_type" {
  description = "Тип создаваемой машины\nhttps://cloud.google.com/compute/docs/general-purpose-machines"
  default = "n1-standard-2"
}


variable "remote_username" {
  type        = string
  description = "Пользователь удалённого хоста"
  default     = "user"
}

variable "region" {
  description = "Регион расположения наших ВМ"
  default = "europe-west3"
}

variable "zone" {
  description = "Зона для региона"
  default = "europe-west3-b"
}

variable "prefix" {
  type        = string
  description = "Префикс для названий ресурсов"
  default     = "test-kuber"
}

variable "tags" {
  type        = list
  description = "Список тэгов"
  default     = ["kube", "test"]
}