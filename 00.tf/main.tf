# Генерируем пару закрытый/открытый ключей
### start
resource "tls_private_key" "global_key" {
  algorithm = "RSA"
  rsa_bits  = 2048
}

resource "local_file" "ssh_private_key_pem" {
  filename          = "${path.module}/id_rsa"
  sensitive_content = tls_private_key.global_key.private_key_pem
  file_permission   = "0600"
}

resource "local_file" "ssh_public_key_openssh" {
  filename = "${path.module}/id_rsa.pub"
  content  = tls_private_key.global_key.public_key_openssh
}
### end



# Генерируем внешние IP
resource "google_compute_address" "static" {
  count = "${var.node_count}"
  name = "${var.project}-ipv4-${count.index}"
}



resource "google_compute_firewall" "fw_allowall" {
  name    = "${var.prefix}-allowall"
  network = "default"

  allow {
    protocol = "all"
  }

  source_ranges = ["0.0.0.0/0"]
  target_tags = "${var.tags}"
}

# resource "google_compute_network" "vpc_network" {
#   name = "${var.prefix}-tf-network"
# }

resource "google_compute_instance" "vm_instance" {
  depends_on = [
    google_compute_firewall.fw_allowall,
  ]

  count         = "${var.node_count}"
  name          = "tf-instance${count.index}"
  machine_type  = "${var.machine_type}"
  zone          = "${var.zone}"

  metadata = {
    ssh-keys = "${var.remote_username}:${tls_private_key.global_key.public_key_openssh}"
  }

  boot_disk {
    initialize_params {
      image     = data.google_compute_image.debian.self_link
      size      = "15"
    }
  }

  network_interface {
    # с наскоку не вышло разобраться вменяемо со своей сетью :-/
    # network     = google_compute_network.vpc_network.name
    network     = "default"
    access_config {
      nat_ip = google_compute_address.static[count.index].address
    }
  }

  tags = "${var.tags}"
}
